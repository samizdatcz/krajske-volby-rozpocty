srovnaniVydaju2 <- function(nazev) {
  
  library(tidyr)
  library(dplyr)
  library(jsonlite)
  options(scipen=999)
  
  selection <- data %>%
    filter(rok > 2012) %>%
    filter(castka > 0) %>%
    select(kraj, rok, druha, castka, obyvatel) %>%
    group_by(kraj, druha, obyvatel) %>%
    summarise(celkem = sum(castka))
  
  selection <- selection  %>%
    mutate(rel = celkem / obyvatel) %>%
    mutate(rel = round(rel, 1)) %>%
    select(kraj, druha, rel)
  
  selection <- spread(selection, kraj, rel)
  selection <- as.data.frame(selection)
  row.names(selection) <- selection[, 1]
  selection <- selection[, -1]
  
  #exportovat první úroveň
  export <-
    data.frame(
      kraj = names(selection),
      y = colSums(selection, na.rm = T),
      stringsAsFactors = F
    )
  
  export <- merge(export, populace)
  export <- data.frame(name=export$kraj, y=export$y, drilldown=export$kod)
  
  export <- export %>%
    arrange(desc(y))
  
  fileConn <- paste0("data/", nazev,".json")
  write("{\"main\":", fileConn, append = T)
  write(toJSON(export), fileConn, append = T)
  
  #exportovat druhou úroveň
  write(",\"drilldown\":[", fileConn, append = T)
  
  counter <- 1
  
  for (k in selection) {
    
    
    write(paste0("{\"name\":\"", names(selection)[counter], "\","), fileConn, append = T)
    write(paste0("\"id\":\"", merge(data.frame(kraj=names(selection)), populace)[counter,2], "\","), fileConn, append = T)
    write("\"data\": [", fileConn, append = T)
    
    counter <- counter + 1
    
    d <- data.frame(polozka=row.names(selection), cena=k, stringsAsFactors = F)
    d <- d[!is.na(d$cena), ] #odstraněny NA
    d <- d[d$cena>0,] #odstraněny nulové položky
    d <- d[order(d$cena, decreasing = T),]
    for (l in 1: nrow(d)) { # přepsat do JSON
      vysledek <- toJSON(list(d[l,1], d[l,2]), auto_unbox = T)
      if (l<nrow(d)) {write(paste0(vysledek, ","), fileConn, append = T)}
      if (l==nrow(d)&&counter<=ncol(selection)) {write(paste0(vysledek, "]},"), fileConn, append = T)}
      if (l==nrow(d)&&counter>ncol(selection)) {write(paste0(vysledek, "]}]}"), fileConn, append = T)}
    }
  }
}