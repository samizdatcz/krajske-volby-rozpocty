library(dplyr)
library(jsonlite)
options(scipen=999)

kraje <- read.csv("data/src/kraje.csv", stringsAsFactors=F)

name <- c("vzdělávání a školské služby", "zdravotnictví", "dopravu", "sociální služby", "vlastní fungování", "kulturu", "tělovýchovu a zájmovou činnost", "ochranu životního prostředí", "požární ochranu", "civilní obranu", "bezpečnost a veřejný pořádek")
treti <- c("Vzdělávání a školské služby", "Zdravotnictví", "Doprava", "Sociální služby a společné činnosti v sociálním zabezpečení a politice zaměstnanosti", "Státní moc, státní správa, územní samospráva a politické strany", "Kultura, církve a sdělovací prostředky", "Tělovýchova a zájmová činnost", "Ochrana životního prostředí", "Požární ochrana a integrovaný záchranný systém", "Civilní připravenost na krizové stavy", "Bezpečnost a veřejný pořádek")
vybrane <- data.frame(name, treti, stringsAsFactors = F)

for (i in 1:nrow(kraje)) {
  fileConn <- paste0("data/", kraje[i, 3],"_2010_2016.json")
  write("[", fileConn, append = T)
  for (j in 1:nrow(vybrane)) {
    casovaRada <- data %>%
      filter(rok<2016) %>%
      filter(kraj==kraje[i, 1]) %>%
      filter(druha==vybrane[j, 2]) %>%
      group_by(rok) %>%
      summarise(celkem=sum(castka))
    casovaRada <- as.data.frame(casovaRada)[, 2]
    casovaRada <-round(casovaRada)
    write(paste0("{\"name\" : \"", vybrane[j, 1], "\","), fileConn, append = T)
    if (j<nrow(vybrane)) {write(paste0("\"data\" : ", toJSON(casovaRada), "},"), fileConn, append = T)}
    if (j==nrow(vybrane)) {write(paste0("\"data\" : ", toJSON(casovaRada), "}]"), fileConn, append = T)}
    print(vybrane[j, 1])
  }
}

#součet všech krajů
fileConn <- paste0("data/vse_2010_2016.json")
write("[", fileConn, append = T)
for (j in 1:nrow(vybrane)) {
  casovaRada <- data %>%
    filter(rok<2016) %>%
    filter(druha==vybrane[j, 2]) %>%
    group_by(rok) %>%
    summarise(celkem=sum(castka))
  casovaRada <- as.data.frame(casovaRada)[, 2]
  casovaRada <-round(casovaRada)
  write(paste0("{\"name\" : \"", vybrane[j, 1], "\","), fileConn, append = T)
  if (j<nrow(vybrane)) {write(paste0("\"data\" : ", toJSON(casovaRada), "},"), fileConn, append = T)}
  if (j==nrow(vybrane)) {write(paste0("\"data\" : ", toJSON(casovaRada), "}]"), fileConn, append = T)}
  print(vybrane[j, 1])
}




  
  